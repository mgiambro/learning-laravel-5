<?php

namespace App\Http\Composers;
use Illuminate\Contracts\View\View;
use App\Repositories\Contracts\ArticleRepositoryInterface;
/**
 * Description of NavigationComposer
 *
 * @author maurizio
 */
class NavigationComposer {
    
    protected $articleRepo;
    
    public function __construct(ArticleRepositoryInterface $articleRepo)
    {
        $this->articleRepo = $articleRepo;
    }
   
    public function compose(View $view)
    {
        $view->with('latest',  $this->articleRepo->latest());
    }
}
