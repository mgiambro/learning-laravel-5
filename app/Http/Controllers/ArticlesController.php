<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\ArticleRepositoryInterface;
use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use App\Repositories\Contracts\TagRepositoryInterface;

class ArticlesController extends Controller {

    protected $articleRepo;
    protected $tagRepo;

    public function __construct(ArticleRepositoryInterface $articleRepo, TagRepositoryInterface $tagRepo)
    {
        $this->articleRepo = $articleRepo;
        $this->tagRepo = $tagRepo;
        $this->middleware('auth', ['only' => 'create']);
    }

    public function index()
    {
        $articles = $this->articleRepo->all();
        $latest = $this->articleRepo->latest();
        return view('articles.index', compact('articles', 'latest'));
    }

    /*
      public function show($id)
      {
      $article = $this->article->find($id);

      return view('articles.show', compact('article'));
      }
     */

    // Usng Route-Model Binding - Use for api's and small projects
    public function show(Article $article)
    {
        return view('articles.show', compact('article'));
    }

    public function create()
    {
        $tags = $this->tagRepo->all();
        return view('articles.create', compact('tags'));
    }

    public function store(ArticleRequest $request)
    {
        // For basic validation use the Illuminate\Http\Request and validate as 
        //$this->validate($request, ['title' => 'required', 'body' => 'required']);

        $input = $request->all();

        $this->articleRepo->create($input);
        /*
          session()->flash('flash_message', 'Your article has been created!');
          session()->flash('flash_message_important', true);
          return redirect('articles')->with([
          'flash_message' => 'Your article has been created!',
          'flash_message_important' => true
          ]);
         */
        //   flash()->success('Your article has been created');
        flash()->overlay('Your article has been created', 'Good Job');
        return redirect('articles');
    }

    /*
      public function edit($id)
      {
      $article = $this->article->find($id);
      return view('articles.edit', compact('article'));
      }
     */

    // Usng Route-Model Binding - Use for api's and small projects
    public function edit(Article $article)
    {
        $tags = $this->tagRepo->all();
        return view('articles.edit', compact('article', 'tags'));
    }

    /*
      public function update(ArticleRequest $request, $id)
      {
      $article = $this->article->find($id);
      $article->update($request->all());

      return redirect('articles');
      }
     */

    // Usng Route-Model Binding - Use for api's and small projects
    public function update(Article $article, ArticleRequest $request)
    {
        $input = $request->all();
        $this->articleRepo->update($article, $input);

        return redirect('articles');
    }

}
