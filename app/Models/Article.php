<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Article extends Model
{
    protected $fillable = [
        'title',
        'body',
        'published_at'
    ];
    
    protected $dates = ['published_at'];
    
    // Query Scopes
    public function scopePublished($query)
    {
        $query->where('published_at', '<=', Carbon::now());
    }
    
      public function scopeUnpublished($query)
    {
        $query->where('published_at', '>', Carbon::now());
    }
    
    // Mutators
    public function setPublishedAtAttribute($date)
    {
        $this->attributes['published_at'] = Carbon::createFromFormat('Y-m-d H:i:s', $date);
    }
   
    public function getPublishedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:s');
    }


    // Relations
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    
    /*
     * Get the tags associated with the given article
     */
    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag')->withTimestamps();
    }
    
    /*
     * Get a list of tag ids associated with the current article.
     */
    public function getTagListAttribute()
    {
        return $this->tags->pluck('id')->toArray();
    }
}
