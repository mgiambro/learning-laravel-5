<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Article;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
   
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Contracts\TagRepositoryInterface', 'App\Repositories\Eloquent\TagRepository');
        $this->app->bind('App\Repositories\Contracts\ArticleRepositoryInterface', 'App\Repositories\Eloquent\ArticleRepository');
       
    }
}
