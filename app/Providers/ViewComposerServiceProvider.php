<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Article;

class ViewComposerServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeNavigation();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function composeNavigation()
    {
        view()->composer('partials._nav', 'App\Http\Composers\NavigationComposer@compose');
//        view()->composer('partials._nav', function($view) {
//            $view->with('latest', Article::latest()->first());
//        });
    }

}
