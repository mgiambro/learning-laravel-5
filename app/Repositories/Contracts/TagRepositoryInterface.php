<?php

namespace App\Repositories\Contracts;

/**
 *
 * @author maurizio
 */
interface TagRepositoryInterface {
    
     public function all();
     
}
