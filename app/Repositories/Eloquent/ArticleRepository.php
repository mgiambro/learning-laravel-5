<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\ArticleRepositoryInterface;
use App\Models\Article;
use Illuminate\Support\Facades\Auth;

/**
 * Description of ArticleRepository
 *
 * @author maurizio
 */
class ArticleRepository implements ArticleRepositoryInterface {

    protected $article;

    public function __construct(Article $article)
    {
        $this->article = $article;
    }
    
    public function latest()
    {
        return $this->article->latest()->first();
    }

    public function all()
    {
        return $this->article->latest('published_at')->published()->get(); // Uses query scopePublished on Article model.
    }

    public function create(array $input)
    {
        return  $this->createArticle($input);
    }

    public function delete($id)
    {
        return $this->article->destroy($id);
    }

    public function find($id, $columns = array('*'))
    {
        return $this->article->findOrFail($id, $columns);
    }

    public function findBy($field, $value, $columns = array('*'))
    {
        return $this->article->where($attribute, '=', $value)->first($columns);
    }

    public function paginate($perPage = 15, $columns = array('*'))
    {
        return $this->article->paginate($perPage, $columns);
    }

    public function update(Article $article, array $data, $id = null)
    {
        //   return $this->article->where($attribute, '=', $id)->update($article->toArray()); // without route-model binding
        $this->syncTags($article, $data['tag_list']);
        return $article->update($data); // with route-model binding
    }

    private function syncTags(Article $article, $tagList)
    {
        $article->tags()->sync($tagList);
    }

    private function createArticle(array $input)
    {
        $article = Auth::user()->articles()->create($input);
        $tagIds = $input['tag_list'];
        $this->syncTags($article, $tagIds);
        //     $article->tags()->attach($tagIds);
        return $article;
    }

}
