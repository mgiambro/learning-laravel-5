<?php


namespace App\Repositories\Eloquent;

use App\Models\Tag;
use App\Repositories\Contracts\TagRepositoryInterface;

/**
 * Description of TagRepository
 *
 * @author maurizio
 */
class TagRepository implements TagRepositoryInterface{
   
    protected $tag;
    
    public function __construct(Tag $tag)
    {
        $this->tag = $tag;
    }

    public function all()
    {
        return $this->tag->pluck('name','id');
    }

}
