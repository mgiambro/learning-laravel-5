const elixir = require('laravel-elixir');
require('laravel-elixir-vue-2');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.sass('app.scss')
            .webpack('app.js');
    
    mix.styles([
        'libs/bootstrap.min.css',
        'app.css',
        'libs/select2.min.css'
    ]);
   

     mix.scriptsIn('resources/assets/js/libs', 'public/js/all.js', 'public/js');
     mix.version(['css/all.css', 'js/all.js']); // Includes an id in the css filename to get round browses using the old css.
//    mix.phpUnit();
});


