<!doctype html>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Learning Laravel 5</title>
        <link rel="stylesheet" href="http://localhost/projects/learning-laravel-5/public{{ elixir('css/all.css') }}">
    </head>
    <body>
        @include('partials/_nav')
        <div class="container">
            @include('flash::message')
            @yield('content')
        </div>
        <script src="http://localhost/projects/learning-laravel-5/public{{ elixir('js/all.js') }}"></script>
        <script>
             $('div.alert').not('.alert-important').delay(3000).slideUp(300);
         // $('#flash-overlay-modal').modal();
        </script>
        @yield('footer')
    </body>
</html>
