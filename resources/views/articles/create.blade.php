
@extends('app')

@section('content')
<h1>Write a new Article</h1>
<hr/>
{!! Form::model($article = new \App\Models\Article, ['url' => 'articles']) !!}
    @include('articles.partials._form', ['submitButtonText' => 'Add Article'])
{!! Form::close() !!}

@include('errors._list')
@stop

